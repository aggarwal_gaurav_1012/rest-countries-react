import React, { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import DarkModeToggle from '../components/DarkModeToggle';
import countriesData from '../countries.json';

const CountryDetailPage = () => {
    const { id } = useParams();
    const [country, setCountry] = useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const navigate = useNavigate();

    useEffect(() => {
        const fetchCountry = () => {
            try {
                const countryData = countriesData.find(country => country.cca3 === id);
                if (!countryData) {
                    throw new Error('Country not found');
                } else {
                    setCountry(countryData);
                }
            } catch (error) {
                setError(error);
            } finally {
                setLoading(false);
            }
        };
        fetchCountry();
    }, [id]);

    const handleBorderClick = (borderId) => {
        navigate(`/country/${borderId}`);
    };

    const getBorderCountryName = (borderCode) => {
        const borderCountry = countriesData.find(country => country.cca3 === borderCode);
        return borderCountry ? borderCountry.name.common : borderCode;
    };

    return (
        <div>
            <header className="header">
                <h1>Where in the world?</h1>
                <DarkModeToggle />
            </header>

            {loading && <div className='waiting-time'>Loading...</div>}
            {error && <div className='waiting-time'>Error: {error.message}</div>}
            {!loading && !error && country && (
                <div className="country-detail">
                    <div>
                        <button className="back-button" onClick={() => navigate(-1)}>
                            <FontAwesomeIcon icon={faArrowLeft} /> Back
                        </button>
                    </div>
                    <div className='country-dtl'>
                        <div>
                            <img src={country.flags.png} alt={`${country.name.common} flag`} />
                        </div>
                        <div className="country-data">
                            <div className='country-name'>
                                <h2>{country.name.common}</h2>
                            </div>
                            <div className='data-text'>
                                <div className='internal-data'>
                                    <p><strong>Native Name:</strong> {Object.values(country.name.nativeName)[0].common}</p>
                                    <p><strong>Population:</strong> {country.population.toLocaleString()}</p>
                                    <p><strong>Region:</strong> {country.region}</p>
                                    <p><strong>Sub Region:</strong> {country.subregion}</p>
                                    <p><strong>Capital:</strong> {country.capital}</p>
                                </div>
                                <div className='internal-data'>
                                    <p><strong>Top Level Domain:</strong> {country.tld.join(', ')}</p>
                                    <p><strong>Currencies:</strong> {Object.values(country.currencies).map(currency => currency.name).join(', ')}</p>
                                    <p><strong>Languages:</strong> {Object.values(country.languages).join(', ')}</p>
                                </div>
                            </div>
                            <div className="borders">
                                <p><strong>Border Countries:</strong> {country.borders ? country.borders.map(border => (
                                    <button key={border} onClick={() => handleBorderClick(border)}>
                                        {getBorderCountryName(border)}
                                    </button>
                                )) : 'None'}</p>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default CountryDetailPage;