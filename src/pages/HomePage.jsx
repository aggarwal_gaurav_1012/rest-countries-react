import React, { useState, useEffect } from 'react';
import CountryList from '../components/CountryList';
import FilterSortSearch from '../components/FilterSortSearch';
import DarkModeToggle from '../components/DarkModeToggle';
import { useNavigate } from 'react-router-dom';
import countriesData from '../countries.json';

const HomePage = () => {
    const [countries, setCountries] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [region, setRegion] = useState('');
    const [subregion, setSubregion] = useState('');
    const [sortOrder, setSortOrder] = useState('');
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const navigate = useNavigate();

    useEffect(() => {
        setLoading(true);
        setCountries(countriesData);
        setLoading(false);
    }, []);

    const filteredCountries = countries.filter(country => {
        return (
            country.name.common.toLowerCase().includes(searchTerm.toLowerCase()) &&
            (region ? country.region === region : true) &&
            (subregion ? country.subregion === subregion : true)
        );
    });

    const sortedCountries = [...filteredCountries].sort((a, b) => {
        if (sortOrder === 'population-asc') {
            return a.population - b.population;
        } else if (sortOrder === 'population-desc') {
            return b.population - a.population;
        } else if (sortOrder === 'area-asc') {
            return a.area - b.area;
        } else if (sortOrder === 'area-desc') {
            return b.area - a.area;
        } else {
            return 0;
        }
    });

    const handleCountryClick = (id) => {
        navigate(`/country/${id}`);
    };

    return (
        <div>
            <header className="header">
                <h1>Where in the world?</h1>
                <DarkModeToggle />
            </header>
            <FilterSortSearch
                setSearchTerm={setSearchTerm}
                setRegion={setRegion}
                setSubregion={setSubregion}
                setSortOrder={setSortOrder}
                region={region}
                countries={countries}
            />
            <div className="main-content">
                {loading && <div className='waiting-time'>Loading...</div>}
                {error && <div className='waiting-time'>Error: {error.message}</div>}
                {!loading && !error && (
                    <>
                        {!sortedCountries.length ? (
                            <div className='not-found'>No such countries found</div>
                        ) : (
                            <CountryList countries={sortedCountries} onCountryClick={handleCountryClick} />
                        )}
                    </>
                )}
            </div>
        </div>
    );
};

export default HomePage;