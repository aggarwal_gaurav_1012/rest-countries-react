import React from 'react';
import { useTheme } from '../contexts/ThemeContext';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMoon as faSolidMoon } from '@fortawesome/free-solid-svg-icons';
import { faMoon as faLightMoon } from '@fortawesome/free-regular-svg-icons';
import '../styles/App.css';

const DarkModeToggle = () => {
  const { darkMode, toggleDarkMode } = useTheme();

  return (
    <button className="dark-mode-toggle" onClick={toggleDarkMode}>
      <FontAwesomeIcon icon={darkMode ? faLightMoon : faSolidMoon} />
      {darkMode ? 'Light Mode' : 'Dark Mode'}
    </button>
  );
};

export default DarkModeToggle;