import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';

const SearchBar = ({ value, onChange }) => {
    return (
        <div className="search-bar">
            <FontAwesomeIcon icon={faMagnifyingGlass} className="search-icon" />
            <input
                type="text"
                value={value}
                placeholder="Search for a country..."
                onChange={(event) => onChange(event.target.value)}
            />
        </div>
    );
};

export default SearchBar;