import React from 'react';

const CountryCard = ({ country, onCountryClick }) => {
    return (
        <div className="country-card" onClick={() => onCountryClick(country.cca3)}>
            <img src={country.flags.png} alt={`${country.name.common} flag`} />
            <h3>{country.name.common}</h3>
            <p>Population: {country.population.toLocaleString()}</p>
            <p>Region: {country.region}</p>
            <p>Capital: {country.capital}</p>
        </div>
    );
};

export default CountryCard;