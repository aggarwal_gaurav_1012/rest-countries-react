import React from 'react';
import CountryCard from './CountryCard';

const CountryList = ({ countries, onCountryClick }) => {
    return (
        <div className="country-list">
            {countries.map(country => (
                <CountryCard key={country.cca3} country={country} onCountryClick={onCountryClick} />
            ))}
        </div>
    );
};

export default CountryList;