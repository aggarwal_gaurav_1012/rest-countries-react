import React, { useEffect } from 'react';
import Dropdown from './Dropdown';
import SearchBar from './SearchBar';

const FilterSortSearch = ({
    searchTerm,
    setSearchTerm,
    region,
    setRegion,
    subregion,
    setSubregion,
    sortOrder,
    setSortOrder,
    countries
}) => {
    const regions = [...new Set(countries.map((country) => country.region))].filter(Boolean);
    const subregions = [...new Set(countries.filter((country) => country.region === region).map((country) => country.subregion))].filter(Boolean);
    const sortOptions = [
        { label: "Population (Ascending)", value: "population-asc" },
        { label: "Population (Descending)", value: "population-desc" },
        { label: "Area (Ascending)", value: "area-asc" },
        { label: "Area (Descending)", value: "area-desc" }
    ];

    useEffect(() => {
        setSubregion('');
        setSortOrder('');
    }, [region, setSubregion, setSortOrder]);

    return (
        <div className="search-filter-container">
            <SearchBar value={searchTerm} onChange={setSearchTerm} />
            <Dropdown options={regions} value={region} onChange={(e) => setRegion(e.target.value)} placeholder="Filter by Region" />
            {region && (
                <Dropdown options={subregions} value={subregion} onChange={(e) => setSubregion(e.target.value)} placeholder="Filter by Subregion" />
            )}
            <Dropdown options={sortOptions.map(option => option.label)} value={sortOrder} onChange={(e) => setSortOrder(sortOptions.find(option => option.label === e.target.value).value)} placeholder="Sort by" />
        </div>
    );
};

export default FilterSortSearch;