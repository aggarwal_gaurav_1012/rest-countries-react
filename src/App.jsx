import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { ThemeProvider } from './contexts/ThemeContext';
import HomePage from './pages/HomePage';
import CountryDetailPage from './pages/CountryDetailPage';
import './styles/App.css';

const App = () => {
    return (
        <ThemeProvider>
            <Router>
                <Routes>
                    <Route path="/" element={<HomePage />} />
                    <Route path="/country/:id" element={<CountryDetailPage />} />
                </Routes>
            </Router>
        </ThemeProvider>
    );
};

export default App;