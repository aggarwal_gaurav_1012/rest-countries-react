# Rest-Countries Project

This project is implemented using React and includes the following functionalities:


- **Search for any country:** Users can search for any country.

- **Fetch data by region and sub-region:** Users can retrieve data based on specific regions and sub-regions.

- **Sort data:** Users can sort countries in ascending or descending order based on population or area.

- **Theme switcher:** Users can switch between light and dark themes.